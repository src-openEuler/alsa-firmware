%define debug_package %{nil}
%define __os_install_post %{nil}
%define _binaries_in_noarch_packages_terminate_build   0
#  ca0132 limite patent license by CTL, so define configure --disable-loader
%define with_loader 0


%if 0%{?with_loader}
%define loader_opt --enable-loader
%define readme hdsploader mixartloader pcxhrloader usx2yloader vxloader ca0132 aica
%else
%define loader_opt  --disable-loader
%define readme usx2yloader aica
%endif

Name:	    alsa-firmware
Version:    1.2.4
Release:    3
Summary:    Firmware for cards

Group:	   Applications/Multimedia
License:   GPLV2
URL:	   http://www.alsa-project.org
Source0:   https://www.alsa-project.org/files/pub/firmware/%{name}-%{version}.tar.bz2

BuildRequires: automake, gcc
Requires:      systemd, alsa-tools-firmware >= 1.1.7
BuildArch:     noarch

%description
alsa-firmware contains binary drivers for various third-party products.

%prep
%autosetup -n %{name}-%{version}

%build
echo %{loader_opt}
%configure %{loader_opt}
make %{?_smp_mflags}
mv aica/license.txt aica/LICENSE
mv aica/Dreamcast_sound.txt aica/README
mv ca0132/creative.txt ca0132/LICENSE

%install
make install DESTDIR=%{buildroot}

# install README COPYING for package  and LICENSE README for third-party products
mkdir -p %{buildroot}%{_docdir}/%{name}
install -c -m 0644 COPYING  %{buildroot}%{_docdir}/%{name}
install -c -m 0644 README  %{buildroot}%{_docdir}/%{name}

for dir in %{readme};
do
  mkdir -p %{buildroot}%{_docdir}/%{name}/$dir
  for f in LICENSE README;
  do
     test -f $dir/$f || continue
     install -c -m 0644 $dir/$f  %{buildroot}%{_docdir}/%{name}/$dir
  done
done
# FIXME: remove non-existing msnd firmware symlinks
rm -rf %{buildroot}/lib/firmware/turtlebeach

%files
%doc  %{_docdir}/%{name}
/lib/firmware/*
%{_datadir}/alsa/firmware/

%changelog
* Mon Jan 11 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1.2.4-3
- Disable strip, because no need to generate debuginfo package

* Wed Jun 30 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1.2.4-2
- add buildrequire gcc

* Sat Jan 30 2021 xinghe <xinghe1@huawei.com> - 1.2.4-1
- upgrade to 1.2.4

* Wed Jul 22 2020 jinzhimin <jinzhimin2@huawei.com> - 1.2.1-1
- upgrade to 1.2.1

* Fri Dec 20 2019 luochunsheng <luochunsheng@huawei.com> - 10.0.29-10
- add requires systemd

* Wed Aug 28 2019 luochunsheng <luochunsheng@huawei.com> - 10.0.29-9
- fix spelling errors

* Mon Aug 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 10.0.29-8
- Package init
